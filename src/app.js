'use strict';

const express = require('express')
const app = express()
const PORT = process.env.PORT || 8080;

app.get('/', (req, res) => res.send('Hello World!'))

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}

// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;

// dummy line
